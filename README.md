# 开放原子MVP实验室

## **开放原子MVP:Labs是什么？**

开放原子MVP（Most Valuable Professional） 实验室是一个开源技术、知识和技能分享和创造的社区协作空间网路，为客户提供了与开放原子基金会MVP专家一起快速启动创新计划和解决业务问题的能力和咨询服务。

![Image description](https://images.gitee.com/uploads/images/2021/0429/195458_773e1b27_8894319.png "开放原子MVP实验室规划概要-20210428_02.png")

## **开放原子MVP:Labs的使命**

开放原子MVP实验室发现并培养开源社区的领袖、开源技术最有价值专家，并组织MVP、开发者、客户一起共同学习、共同开发、共同创造、共同想象、共同成就、共同反思，使用开放源代码一起解决复杂的问题，旨在提升社会的创新能力和加速企业的数字化转型。

## **开放原子MVP:Labs的理念**

使用参与式和基于社区驱动创新的方法，组织和激励为开源社区作出杰出贡献的顶尖技术专家与客户一起合作，引导企业用开源协作方式解决各种业务问题，并能够构建创新驱动的优势。MVP:Lab将成为面向为未来协同经济的价值交换网络。

## **开放原子MVP:Labs的发展目标**

- 发展和培养开源领域的MVP：为开源社区的开发者提供创新的学习方法和激励计划，帮助他们成长为开源社区最具有影响力和最有价值的技术专家。他们由追求获得新的、令人兴奋的技术和知识热情所驱动，充满了乐于分享的社区精神。
- 创造最佳的开放式协作体验：设计和创造让开发者身临其境的模拟实验室协作体验，使MVP和客户沉浸于开放式协作、开放式技术和快速敏捷的创新交付方式场景中，使他们能够学习如何快速、开发和高效地交付创新成果。
- 促进在社区中的合作关系：鼓励MVP与客户并肩工作，采用开源技术和开放标准的最佳敏捷实践，从发现/alpha/beta/现场交付业务成果，从而以更快的方式催化创新并解决业务挑战。

## **开放原子MVP:Labs的主要功能**

- Co-Learning: MVPLabs提供了一个聚集了开发者和专家的社区，以交流有关开源技术关键要素的知识。该社区将通过讲座和互动式会议来快速追踪开发者的学习情况，将理论与实践联系起来的案例研究方法，以及动手练习，以将学习内容应用到开发者自己的环境中。
- Co-Mentoring：MVPLabs提倡建立一种新型的互相指导的伙伴性学习或协作关系，社区中的成员和MVP一起肩并肩合作，学习和发展新技能。MVPLabs为导师提供支持和培训，帮助制定总体指导计划，并为指导小组提供交流和促进策略方面的建议。 
- Co-Development：通过MVPLaps，客户的IT团队可以与MVP、开发者社区密切合作，快速跟踪从传统工作方式向适应性，开放性和创新性的过渡。,使现有的应用程序现代化，或者使用开源和敏捷开发创建新的应用程序或者解决方案。

## **开放原子MVP:Labs如何工作？**

开放原子MVPLaps为客户团队提供了MVP专家、开放源代码工具以及受开放源代码启发的实践流程，以构建应用程序原型并快速，迭代地提供业务价值。

在MVPLaps的开放协作的实验环境中，我们与客户合作以：

- 共同创建创新且对业务至关重要的软件：快速构建原型，采用敏捷开发，从第一天开始就使用预先连接的基础架构而变得敏捷。
- 沉浸式协作体验：在虚拟环境中与MVP专家并肩工作，以确保成功使用和采用开放源代码工具以及开放式实践来交付业务价值。
- 建立新的工作方式：从开放实践库中成熟的实践中汲取灵感，并为客户的团队带来现代的应用程序开发方法。
- 在不确定的环境中蓬勃发展：我们引入工具和技术来创建在虚拟环境中可能面临挑战的高性能团队。 

## **开放原子MVP:Labs的交付模式**

OpenAtom MVPLabs的交付模式包括：

- 分布在开放原子开源基金会的办公场地
- 由开放原子开源基金会和授权的合作伙伴共建
- 以主题工作坊、实训营、黑客松等计划为客户提供创新实践

